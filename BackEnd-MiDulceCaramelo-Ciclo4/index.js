const app = require("./app");
const port = 4000;
const mongo = require("./database");
const CotiRouter = require("./src/routers/Cotizacion.routes")
const ProdRouter = require("./src/routers/Productos.routes");
const UserAdminRouter = require("./src/routers/Usuarios.routes");
const cors = require('cors');

app.use(cors());

app.listen(port, () => {
    console.log("servidor corriendo ok puerto: " + port);
});

app.get("/", (req, res) => {
    res.send("API Funcionando");
});

app.use("/api/Product", ProdRouter);
app.use("/api/Cotizacion", CotiRouter);
app.use("/api/UserAdm", UserAdminRouter);