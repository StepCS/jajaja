const mongoose = require("mongoose");

const userAdminSchema = new mongoose.Schema(
    {
        idAdm: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true            
        }
    },
    {
        timestamps: true,
        versionKey: false
    }
);



const UserAdm = new mongoose.model("UserAdm", userAdminSchema);
module.exports = UserAdm;