const mongoose = require("mongoose");

const productoSchema = new mongoose.Schema(
    {
        idProd: {
            type: String,
            required: true
        },
        category: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        img: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
    },
    {
        timestamps: true,
        versionKey: false
    }
);


const Producto = new mongoose.model("Producto", productoSchema);
module.exports = Producto;