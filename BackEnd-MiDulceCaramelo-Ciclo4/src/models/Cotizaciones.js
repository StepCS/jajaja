const mongoose = require("mongoose");

const cotizacionSchema = new mongoose.Schema(
    {
        address: {
            type: String,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        lastName: {
            type: String,
            required: true,
        },
        nameUs: {
            type: String,
            required: true,
        },
        phone: {
            type: String,
            required: true,
        },
        orderId: {
            type: String,
            required: true,
        },
        producto: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Producto',
            autopopulate: true
        }],
        amount:{
           type: Number,
            required: true,
        },
        total: {
           type: Number,
            required: true,
        }
    },
    {
        timestamps: true,
        versionKey: false
    }
);

cotizacionSchema.plugin(require('mongoose-autopopulate'));

const Cotiz = new mongoose.model("Corizacion", cotizacionSchema);
module.exports = Cotiz;