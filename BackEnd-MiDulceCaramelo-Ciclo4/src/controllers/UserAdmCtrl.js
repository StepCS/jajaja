const UserAdm = require("../models/UserAdm");
const jwt = require('jsonwebtoken');

const admSave = async (req, res) => {
    try {
        const { email } = req.body;
        let adm = await UserAdm.findOne({ email });

        if (adm) {
            return res.status(400).json({ msg: "Ese correo ya existe" });
        }
        else{
            adm = new UserAdm(req.body);
            await adm.save();
            return res.status(200).json({ msg: "Admin creado correctamente" });
        }
        const payload = {
            adm: { _id: adm.id },
        };

        jwt.sign(
            payload,
            process.env.SECRETA,
            {
                expiresIn: 3600, //1 hora
            },
            (error, token) => {
                if (error) throw error;

                //Mensaje de confirmación
                res.json({ token });
            }
        );
    }catch (error){
        console.error(error);
    }
}


const admList = async (req, res) => {
    try {
        let userList = await UserAdm.find();
        res.status(200).send(userList)
    } catch (error) {
        console.error(error);
    }
}

const admId = async (req, res) => {
    try {
        const id = req.params.id;
        if(id){
            let adm = await UserAdm.findById(id);
            res.status(200).send(adm);
        }
        else{
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const admDel = async (req, res) => {
    try {
        const id = req.params.id;
        if(id){
            let adm = await UserAdm.findByIdAndDelete(id);
            res.send("UserAdm eliminado correctamente");
        }
        else{
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const admMod = async (req, res) => {
    try {
        const id = req.params.id;
        let adm = req.body;
        await UserAdm.findByIdAndUpdate(id, adm);
        res.send("UserAdm actualizado correctamente");
    }catch (error) {
        console.error(error);
    }
}

//login

const admLogin = async (req, res) => {
    try {
        const { email, password } = req.body;
        const adm = await UserAdm.findOne({ email });
        if(adm){
            if(adm.password === password){
                return res.status(200).json({
                    msg: "Ingreso",
                    adm: adm.id
                });
            }
            else{
                return res.status(400).json({ mensaje: "Denegado" });
            }
        }
        else{
            return res.status(400).json({ mensaje: "Denegado" });
        }

        //FIRMAR EL TOKEN
        const payload = {
            adm: { id: adm.id },
        };

        jwt.sign(
            payload,
            process.env.SECRETA,
            {
                expiresIn: 3600, //1 hora
            },
            (error, token) => {
                if (error) throw error;

                //Mensaje de confirmación
                res.json({ token });
            }
        );

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    admSave,
    admList,
    admId,
    admDel,
    admMod,
    admLogin
}