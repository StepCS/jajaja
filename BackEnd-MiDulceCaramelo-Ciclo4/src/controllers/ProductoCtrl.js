const Producto = require("../models/Productos");
const jwt = require("jsonwebtoken");


const prodSave = async (req, res) => {
    try {
        const { idProd } = req.params;

        let product = await Producto.findOne({ id: idProd});
        if(product){
            return res.status(400).json({ msg : "Producto existente"});
        }else {
            product = new Product(req.body);
            await Producto.save();
            return res.status(200).json({ msg : "Producto creado"});
        }

        const payload={
            product:{id: product.id, }
        }

        jwt.sign(
            payload,
            process.env.SECRETA,
            {
                expiresIn : 3600,
            },
            (error, token) =>{
                if (error) throw error;
                res.json({token})
            });
    }catch(error){
        console.error(error);
    }

}

const producList = async (req, res) => {
    try {
        let prodList = await Producto.find();
        res.status(200).send(prodList)
    } catch (error) {
        console.error(error);
    }
}

const prodId = async (req, res) => {
    try {
        const id = req.params.id;
        if(id){
            let prod = await Producto.findById(id);
            res.status(200).send(prod);
        }
        else{
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const prodDel = async (req, res) => {
    try {
        const id = req.params.id;
        if(id){
            let prod = await Producto.findByIdAndDelete(id);
            res.send("Producto eliminado correctamente");
        }
        else{
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const prodMod = async (req, res) => {
    try {
        const id = req.params.id;
        const producto = req.body;
        await Producto.findByIdAndUpdate(id, producto);
        res.send("Producto actualizado correctamente");
    }catch (error) {
        console.error(error);
    }
}

const prodParam = async (req, res) => {
    try {
        const param = req.params.param;
        const product = await Producto.find({
            $or: [
            {idProd: { $regex: '.*' + param + '.*' }},
            {name: { $regex: '.*' + param + '.*' }},
            {category: { $regex: '.*' + param + '.*' }}
        ]
        });
        res.status(200).send(product)
    }catch (error) {
        console.error(error);
    }
}

module.exports = {
    prodSave,
    producList,
    prodId,
    prodDel,
    prodMod,
    prodParam
}