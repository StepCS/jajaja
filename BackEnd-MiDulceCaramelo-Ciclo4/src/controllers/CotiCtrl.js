const Cotizacion = require("../models/Cotizaciones");
const Producto = require("../models/Productos");

const cotiSave = async (req, res) => {
    try {
        let coti = new Cotizacion(req.body);
        await coti.save();
        res.send("Cotizacion creado correctamente")
    } catch (error) {
        console.error(error);
    }
}

const cotiList = async (req, res) => {
    try {
        let cotiLisst = await Cotizacion.find();
        res.status(200).send(cotiLisst);
    } catch (error) {
        console.error(error);
    }
}

const idCoti = async (req, res) => {
    try {
        const id = req.params.id;
        if (id) {
            let coti = await Cotizacion.findById(id);
            res.status(200).send(coti);
        }
        else {
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const cotDel = async (req, res) => {
    try {
        const id = req.params.id;
        if (id) {
            let coti = await Cotizacion.findByIdAndDelete(id);
            res.send("Cotizacion eliminada correctamente");
        }
        else {
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const cotiMod = async (req, res) => {
    try {
        const id = req.params.id;
        let cotizacion = req.body;
        await Cotizacion.findByIdAndUpdate(id, cotizacion);
        res.send("Cotizacion actualizado correctamente");
    } catch (error) {
        console.error(error);
    }
}

const cotiAddPro = async (req, res) => {
    try {
        const id = req.params.id;
        const { producto } = req.body;
        await Cotizacion.findByIdAndUpdate(id, {
            $push: { producto: producto },
        });
        res.send("Cotizacion actualizado correctamente");
    } catch (error) {
        console.error(error);
    }
}

const cotDelProd = async (req, res) => {
    try {
        const id = req.params.id;
        const { producto } = req.body;
        await Cotizacion.findByIdAndUpdate(id, {
            $pull: { producto: producto },
        });
        res.send("Cotizacion actualizado correctamente");
    } catch (error) {
        console.error(error);
    }
}
const cotParam = async (req, res) => {
    try {
        const cot = req.params.cot;
        const cotiz = await Cotizacion.find({
            $or: [
                {orderId: { $regex: '.*' + param + '.*' }},
                {nameUs: { $regex: '.*' + param + '.*' }}
            ]
        });
        res.status(200).send(cotiz)
    }catch (error) {
        console.error(error);
    }
}

module.exports = {
    cotiSave,
    cotiList,
    idCoti,
    cotDel,
    cotiMod,
    cotDelProd,
    cotiAddPro,
    cotParam
}