const { Router } = require("express");

const CotiRouter = Router();
const CotiCtrl = require("../controllers/CotiCtrl");

CotiRouter.post("/new", CotiCtrl.cotiSave);
CotiRouter.get("/list", CotiCtrl.cotiList);
CotiRouter.get("/list/:id", CotiCtrl.idCoti);
CotiRouter.delete("/del/:id", CotiCtrl.cotDel);
CotiRouter.put("/act/:id", CotiCtrl.cotiMod);
CotiRouter.put("/addProd/:id", CotiCtrl.cotiAddPro);
CotiRouter.put("/delProd/:id", CotiCtrl.cotDelProd);
CotiRouter.get('/list/:param', CotiCtrl.cotParam)

module.exports = CotiRouter;
