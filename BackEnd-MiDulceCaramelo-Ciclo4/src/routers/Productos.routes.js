const { Router } = require("express");

const ProdRouter = Router();
const ProdCtrl = require("../controllers/ProductoCtrl");

ProdRouter.post("/new", ProdCtrl.prodSave);
ProdRouter.get("/list", ProdCtrl.producList);
ProdRouter.get("/list/:id", ProdCtrl.prodId);
ProdRouter.delete("/del/:id", ProdCtrl.prodDel);
ProdRouter.put("/act/:id", ProdCtrl.prodMod);
ProdRouter.get('/lsit/:param', ProdCtrl.prodParam)
module.exports = ProdRouter;
