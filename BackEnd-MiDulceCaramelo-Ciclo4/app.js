const express = require("express");
const app = express();
//var bodyParser = require("body-parser");
const methodOverride = require("method-override");
const mongoose = require("mongoose");
app.use(express.json());
app.use(express.urlencoded({
    extended: true,
})
);
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, x-auth-token, X-API-KEY, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, HEAD, POST, OPTIONS, PUT, DELETE");
    res.header("Allow", "GET, HEAD, POST, OPTIONS, PUT, DELETE");
    next();
});
module.exports = app;