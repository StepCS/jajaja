import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import {APIInvoke} from '../../utils/ApiInvoke';
import swal from "sweetalert";
const api = new APIInvoke();

const NewAccount = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const [admin, setAdmin] = useState({
        admId: "",
        email: "",
        password: ""
    })

    const { admId, email, password } = admin;

    const onChange = (e) => {
        setAdmin({
            ...admin,
            [e.target.name]: e.target.value
        })
    };

    useEffect(() => {
        document.getElementById("admId").focus();
    }, []);

    const newAccount = async () => {
        const data = {
            idAdm: admin.admId,
            email: admin.email,
            password: admin.password
        }
        
        const response = await api.invokePOST(
            "/api/UserAdm/new", data);
            console.log(response);
        const rest = response.mensaje;
        let titulo, msg, tipo;
        if (rest === "Admin Existente") {
            titulo = "Error al crear el admin";
            msg = "El correo ya existe";
            tipo = "error";
            alerta(msg, tipo, titulo);
        } else if (rest === "Admin creado") {
            titulo = "Proceso Exitoso!";
            msg = "Admin Creado correctamente";
            tipo = "success";
            alerta(msg, tipo, titulo);
        }
        setAdmin({
            admId: "",
            email: "",
            password: ""
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        newAccount();
    };



    return (
        <div className="container">
            <div className="row-10 mt-5 mx-auto">
                <div className="col">
                </div>
                <div className="col">
                    <div className="card text-center">
                        <div className="card-header">
                            Crear correo administrativo
                        </div>
                        <div className="card-body">
                            <form onSubmit={onSubmit}>
                                <div>
                                    <div className="form-floating mb-2">
                                        <input 
                                            type="text" 
                                            className="form-control" 
                                            id="admId"
                                            placeholder="Id admin"
                                            name = "admId"
                                            value={admId}
                                            onChange={onChange}
                                            required />
                                        <label htmlFor="floatingInput">Id Admin</label>
                                    </div>
                                    <div className="form-floating mb-2">
                                        <input 
                                            type="email" 
                                            className="form-control" 
                                            id="email"
                                            placeholder="name@example.com"
                                            name='email'
                                            value={email}
                                            onChange={onChange}
                                            required />
                                        <label htmlFor="floatingInput">Correo Electronico</label>
                                    </div>
                                    <div className="form-floating">
                                        <input 
                                            type="password" 
                                            className="form-control" 
                                            id="password"
                                            placeholder="Password"
                                            name="password"
                                            value={password}
                                            onChange={onChange}
                                            required />
                                        <label htmlFor="floatingPassword">Contraseña</label>
                                    </div>
                                </div>
                                <div className='container mt-5'>
                                    <button type="submit" className="btn btn-success mx-2">Crear</button>
                                    <Link to={"/Login"} className="btn btn-secondary mx-2">Tengo Cuenta</Link>
                                </div>
                            </form>
                        </div>
                        <div className="card-footer text-muted">
                            Diligenciar todos los campos
                        </div>
                    </div>
                </div>
                <div className="col-2">
                </div>
            </div>
        </div>
    )
}

export default NewAccount;