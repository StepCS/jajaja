import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { APIInvoke } from '../../utils/ApiInvoke';
import swal from 'sweetalert';
import { useNavigate } from 'react-router-dom';
const api = new APIInvoke();

const Login = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const naveg = useNavigate();

    const [adm, setAdmin] = useState({
        email: "",
        password: ""
    });
    const { email, password } = adm;

    const onChange = (e) => {
        setAdmin({
            ...adm,
            [e.target.name]: e.target.value
        })
    };

    useEffect(() => {
        document.getElementById("email").focus();
    }, []);

    const admLogin = async () => {

        const data = {
            email: adm.email,
            password: adm.password
        }
        const resp = await api.invokePOST("/api/UserAdm/login", data);
        console.log(resp);

        const access = resp.msg;
        let titulo, mensaje, tipo;
        if (access === "Ingreso") {
            titulo = "Proceso Exitoso!";
            mensaje = "Ingreso exitoso al sistema";
            tipo = "success";
            alerta(mensaje, tipo, titulo);

            localStorage.setItem("admin", resp.adm);

            naveg("/Home");
        }
        else if (access === "Denegado") {
            titulo = "Acceso Denegado";
            mensaje = "Usuario o contraseña incorrectos";
            tipo = "error";
            alerta(mensaje, tipo, titulo);
        }

        setAdmin({
            email: "",
            password: ""
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        admLogin();
    };

    return (

        <div className="container">
            <div className="row-10 mt-5 mx-auto">
                <div className="col">

                </div>
                <div className="col">
                    <div className="card text-center">
                        <div className="card-header">
                            Ingreso Administrativo
                        </div>
                        <div className="card-body">
                            <form onSubmit= {onSubmit}>
                                <div>
                                    <div className="form-floating mb-3">
                                        <input
                                            id="email"
                                            type="email"
                                            className="form-control"
                                            placeholder="Correo Electronico"
                                            name="email"
                                            value={email}
                                            onChange={onChange}
                                            required
                                        />
                                        <label htmlFor="floatingInput">Correo Electronico</label>
                                    </div>
                                    <div className="form-floating">
                                        <input
                                            type="password"
                                            className="form-control"
                                            id="password"
                                            placeholder="Contraseña"
                                            name="password"
                                            value={password}
                                            onChange={onChange}
                                            required
                                        />
                                        <label htmlFor="floatingPassword">Contraseña</label>
                                    </div>
                                </div>

                                <div className='container mt-3'>
                                    <button type="submit" className="btn btn-success mx-2">Iniciar Sesión</button>
                                    <Link to={"/NewAccount"} className="btn btn-secondary mx-2">Crear Cuenta</Link>
                                </div>

                            </form>
                        </div>

                        <div className="card-footer text-muted">
                            Olvide la contraseña 🥲
                        </div>
                    </div>
                </div>
                <div className="col-2">

                </div>
            </div>

        </div>


    )
}

export default Login;