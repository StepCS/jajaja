import React from "react";
import { useEffect, useState } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import { confirm } from "react-confirm-box"
const api = new APIInvoke();

const ProductList = () => {
    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const [products, setProduct] = useState([]);

    const loadProduct = async () => {
        const response = await api.invokeGET("/api/Product/list");
        console.log(response);
        setProduct(response);
    }

    useEffect(() => {
        loadProduct();
    }, [])

    const onSubmit = (e) => {
        e.preventDefault();
        const parametro = document.getElementById("param").value;
    }

    return (
        <div className="product-card__wrapper">
            <div className="product-card">
                <div className="product__image__wrapper">
                    <a href="product-single.html" className="product__image">
                        <img src="assets/images/products/prod-01.png" alt="icon" />
                    </a>
                    <div className="product__actions">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#prod__preview" className="action__btn">
                            <svg width={16} height={16} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.666992 7.99996C0.666992 7.99996 3.33366 2.66663 8.00033 2.66663C12.667 2.66663 15.3337 7.99996 15.3337 7.99996C15.3337 7.99996 12.667 13.3333 8.00033 13.3333C3.33366 13.3333 0.666992 7.99996 0.666992 7.99996Z" stroke="#252522" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M8 10C9.10457 10 10 9.10457 10 8C10 6.89543 9.10457 6 8 6C6.89543 6 6 6.89543 6 8C6 9.10457 6.89543 10 8 10Z" stroke="#252522" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </a>
                        <a href="#" className="action__btn">
                            <svg width={16} height={16} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 9.33329L13.3333 5.99996L10 2.66663" stroke="#252522" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M2.66699 13.3333V8.66667C2.66699 7.95942 2.94794 7.28115 3.44804 6.78105C3.94814 6.28095 4.62641 6 5.33366 6H13.3337" stroke="#252522" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </a>
                    </div>
                </div>
                <div className="product__content">
                    <div className="product__title">
                        <h5><a href="product-single.html">Producto</a></h5>
                    </div>
                    <div className="product__bottom">
                        <div className="product__price">
                            $22.00
                            <del>$22.00</del>
                        </div>
                        <div className="cart__action__btn">
                            <div className="cart__btn">
                                <a href="#" className="btn btn-outline">Agregar al carrito</a>
                            </div>
                            {/* <div className="quantity cart__quantity">
                                <button type="button" className="decressQnt">
                                    <span className="bar" />
                                </button>
                                <input className="qnttinput" type="number" disabled defaultValue={0} min={01} max={100} />
                                <button type="button" className="incressQnt">
                                    <span className="bar" />
                                </button>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
export default ProductList;