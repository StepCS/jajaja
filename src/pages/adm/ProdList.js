import React from "react";
import { useEffect, useState } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import { confirm } from "react-confirm-box"
const api = new APIInvoke();


const ListProd = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const [product, setProduct] = useState([]);

    const loadProduct = async () => {
        const response = await api.invokeGET("/api/Product/list");
        console.log(response);
        setProduct(response);
    }

    useEffect(() => {
        loadProduct();
    }, [])

    const delProduct = async (e, id) => {
        e.preventDefault();

        const confirmar = await confirm("Quere eliminar este producto?");
        let msj, titulo, tipo;

        if (confirmar) {
            const response = await api.invokeDELETE("/api/Productos/del/" + id);
            console.log(response.mensaje);

            msj = "Producto eliminado";
            tipo = "success";
            titulo = "proceso exitoso"
            alerta(msj, titulo, tipo);
        } else {
            msj = "Error al eliminar";
            tipo = "warning";
            titulo = "Advertisement"
            alerta(msj, titulo, tipo);
        }
    };

    const onSubmit = (e) => {
        e.preventDefault();
        const param = document.getElementById("param").value;
        console.log(param)
    }
    console.log(product);
    return (
        <div class="header__dashboard">
            <div class="main__container">
                <div class="main__inner--wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="dashboard__breadcrumb">
                                <div class="breadcrumb__meta">
                                </div>
                            </div>
                            <div class="dashboard__header">
                                <div class="d__header--left">
                                    <h2 class="title">Lista de productos</h2>
                                </div>
                                <div class="add__btn">
                                    <Link to={"/NewProd"} className="btn btn-secondary mb-3 mx-2" id="botonNewP">Producto Nuevo</Link>
                                </div>
                            </div>
                            <div class="dashboard__search--filter">
                                <form onSubmit={onSubmit}>
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            className="form-control mx-5 mb-4"
                                            id="parametro"
                                            name="parametro"
                                            placeholder="Ingrese Id del producto o el nombre"
                                            aria-describedby="inputGroupFileAddon04"
                                            aria-label="Upload"
                                        />
                                        <button class="btn btn-outline-secondary" type="button" id="button-addon2">Buscar</button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-body shadow">
                                <div class="table-responsive">
                                    <table class="table dashboard__table order-list">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-start">
                                                    <div class="heading">
                                                        Id Producto
                                                        <div class="icon"><i class="fa-solid fa-arrow-down"></i></div>
                                                    </div>
                                                </th>
                                                <th scope="col" class={"text-center"}>Nombre</th>
                                                <th scope="col" class="text-center">Categoria</th>
                                                <th scope="col" class="text-start">Descipción</th>
                                                <th scope="col" class="text-center">Precio</th>
                                                <th scope="col" class="text-center">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                product.map(
                                                    item =>
                                                        <tr>
                                                            <th scope="row">{item.idProd}</th>
                                                            <td>{item.name}</td>
                                                            <td>{item.category}</td>
                                                            <td>{item.description}</td>
                                                            <td>{item.price}</td>
                                                            <td>
                                                                <Link class="btn btn-outline-success mx-3"
                                                                    to={`/api/Product/act/${item._id}`} >
                                                                    Actualizar
                                                                </Link>
                                                                <button
                                                                    class="btn btn-outline-secondary"
                                                                    onClick={(e) => delProduct(e, item._id)}>
                                                                    Eliminar
                                                                </button></td>
                                                        </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ListProd;