import React, {Fragment} from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "../src/pages/auth/Login";
import NewAccount from "../src/pages/auth/NewAccount";
import Home from "../src/pages/Home";
import ProdList from "../src/pages/adm/ProdList";
import ProductClients from "../src/pages/cliente/ProductsClients";


function App() {
  return (
    <Fragment>
        <Router>
            <Routes>
                <Route path="/Login" exact element={<Login/>}/>
                <Route path="/NewAccount" exact element={<NewAccount/>}/>
                <Route path="/Home" exact element={<Home/>}/>
                <Route path="/ProdList" exact element={<ProdList/>}/>
                <Route path="/Products" exact element={<ProductClients/>}/>

            </Routes>
        </Router>
    </Fragment>
  );
}

export default App;
